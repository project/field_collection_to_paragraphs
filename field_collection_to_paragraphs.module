<?php

/**
 * @file
 * Field Collection to Paragraphs module functions.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Utility\UpdateException;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Implements hook_help().
 */
function field_collection_to_paragraphs_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the field_collection_to_paragraphs module.
    case 'help.page.field_collection_to_paragraphs':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Migration for Field Collections to Paragraphs.') . '</p>';
      return $output;
  }

  return NULL;
}

/**
 * Provides an all-in-one method of executing the necessary logic.
 *
 * This allows Field Collection fields to be migrated one at a time and can be
 * used in hook_post_update_NAME().
 *
 * @param array $field_names
 *   A list of Field Collection fields that will be converted.
 * @param string $suffix
 *   The suffix to be added to the Paragraphs field, defaults to "_paragraph".
 * @param bool $purge_old
 *   Purge the existing tables, useful for when running the function multiple
 *   times for multiple fields.
 *
 * @throws \Drupal\Core\Utility\UpdateException
 *
 * @todo Update to support the update sandbox system.
 */
function field_collection_to_paragraphs_update_script_wrapper(array $field_names = [], $suffix = '_paragraph', $purge_old = TRUE) {
  // If Field Collection is not installed there's nothing to do.
  /** @var \Drupal\Core\Extension\ExtensionList $module_extension_list */
  $module_extension_list = \Drupal::service('extension.list.module');
  if (!$module_extension_list->exists('field_collection')) {
    throw new UpdateException(t('The Field Collection module is not installed, nothing to do.'));
  }

  // Optionally empty the existing tables.
  if ($purge_old) {
    $tables = [
      'migrate_map_field_collection_to_paragraph_field_entity_form_dis',
      'migrate_map_field_collection_to_paragraph_field_entity_view',
      'migrate_map_field_collection_to_paragraph_field_instance',
      'migrate_map_field_collection_to_paragraph_field_storage',
      'migrate_map_field_collection_to_paragraph_item',
      'migrate_map_field_collection_to_paragraph_parent_field_entity_f',
      'migrate_map_field_collection_to_paragraph_parent_field_entity_v',
      'migrate_map_field_collection_to_paragraph_parent_field_instance',
      'migrate_map_field_collection_to_paragraph_parent_field_storage',
      'migrate_map_field_collection_to_paragraph_parent_node',
      'migrate_map_field_collection_to_paragraph_parent_taxonomy_term',
      'migrate_map_field_collection_to_paragraph_type',
    ];
    $connection = \Drupal::database();
    foreach ($tables as $table) {
      if ($connection->schema()->tableExists($table)) {
        $connection->query("TRUNCATE {$table};");
      }
    }
  }

  // Load the configuration and change it to only process a single field.
  $config_factory = \Drupal::configFactory();
  $config = $config_factory->getEditable('migrate_plus.migration_group.field_collection_to_paragraphs');
  $config->set('shared_configuration.source.field_collection_types', $field_names);
  $config->set('shared_configuration.source.field_collection_field_name_suffix', $suffix);
  $config->save(TRUE);

  // Run the migrations in this order.
  $migrations = [
    'field_collection_to_paragraph_type',
    'field_collection_to_paragraph_field_storage',
    'field_collection_to_paragraph_field_instance',
    'field_collection_to_paragraph_field_entity_form_display',
    'field_collection_to_paragraph_field_entity_view',
    'field_collection_to_paragraph_parent_field_storage',
    'field_collection_to_paragraph_parent_field_instance',
    'field_collection_to_paragraph_item',
    'field_collection_to_paragraph_parent_field_entity_form_display',
    'field_collection_to_paragraph_parent_field_entity_view',
    'field_collection_to_paragraph_parent_node',
    'field_collection_to_paragraph_parent_taxonomy_term',
  ];
  $migration_manager = \Drupal::service('plugin.manager.migration');

  foreach ($migrations as $ctr => $migration_id) {
    $migration = $migration_manager->createInstance($migration_id);
    $migration->setStatus(MigrationInterface::STATUS_IDLE);
    $executable = new MigrateExecutable($migration);
    $result = $executable->import();
    if ($result === MigrationInterface::RESULT_COMPLETED) {
      \Drupal::messenger()->addStatus(t("Successfully completed step #@count: @migration", [
        '@count' => $ctr + 1,
        '@migration' => $migration_id,
      ]));
    }
    else {
      throw new UpdateException(t("Migration {$migration} did not finish properly."));
    }
  }
}
